import React from 'react';
import { Switch, Route } from 'react-router-dom';

const Home = React.lazy(() => import(/* webpackChunkName: "Home" */'./views/Home' ));

const AppRoutes = () => (
    <Switch>
        <Route path="/" exact component={Home}/>
    </Switch>
);

export default AppRoutes;
