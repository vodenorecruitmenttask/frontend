import {ICategory} from '../models/ICategory';
import {IAction} from '../models/IAction';

export const ACTION_TYPES = {
    GET_CATEGORIES_SUCCESS: 'categories/SUCCESS',
    GET_CATEGORIES_FAILURE: 'categories/FAILURE',
    RESET: 'categories/RESET'
};

export type CategoryReducerState = {
    data: ICategory[];
    error: string | null;
}

const initialState: CategoryReducerState = {
    data: [],
    error: null
}

export default (state = initialState, action: IAction): CategoryReducerState => {
    switch (action.type) {
        case (ACTION_TYPES.GET_CATEGORIES_SUCCESS):
            return {
                ...state,
                data: [...action.payload]
            };
        case (ACTION_TYPES.GET_CATEGORIES_FAILURE):
            return {
                ...state,
                data: [],
                error: action.payload
            }
        case (ACTION_TYPES.RESET):
            return {
                ...initialState
            };
        default:
            return state;
    }
}