import {combineReducers} from 'redux';
import flow from './flowReducer';
import categories from './categoryReducer';

export default combineReducers({
    flow,
    categories
})