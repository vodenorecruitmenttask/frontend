import {IFlow} from '../models/IFlow';
import {IAction} from '../models/IAction';

export const ACTION_TYPES = {
    SET_FLOW: 'flowReducer/SET_FLOW',
    SET_ACTION_FLOW: 'flowReducer/SET_ACTION_FLOW',
    RESET_ACTION_FLOW: 'flowReducer/RESET_ACTION_FLOW',
    RESET: 'flowReducer/RESET'
};

export type FlowReducerState = {
    data: IFlow;
    categoryName: string;
    actionName: string;
}

const initialState: FlowReducerState = {
    data: {},
    categoryName: '',
    actionName: ''
}

export default (state = initialState, action: IAction): FlowReducerState => {
    switch (action.type) {
        case (ACTION_TYPES.SET_FLOW):
            return {
                ...state,
                data: action.payload.flow,
                categoryName: action.payload.categoryName
            };
        case (ACTION_TYPES.SET_ACTION_FLOW):
            return {
                ...state,
                data: action.payload.flow,
                actionName: action.payload.actionName
            };
        case (ACTION_TYPES.RESET_ACTION_FLOW):
            return {
                ...state,
                actionName: ''
            };
        case (ACTION_TYPES.RESET):
            return {
                ...initialState
            };
        default:
            return state;
    }
}