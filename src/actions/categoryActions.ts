import {Dispatch} from 'redux';
import {ICategory} from '../models/ICategory';
import {ACTION_TYPES} from '../reducers/categoryReducer';

const api = 'http://localhost:3001';

const setCategories = (categories: ICategory[]) => {
    return {
        type: ACTION_TYPES.GET_CATEGORIES_SUCCESS,
        payload: categories
    };
}

const setErrorMessage = (message: string | null) => {
    return {
        type: ACTION_TYPES.GET_CATEGORIES_FAILURE,
        payload: message
    }
}

export const getCategoriesAction = () => {
    const requestUrl = `${api}/categories`;
    return async (dispatch: Dispatch) => {
        return fetch(requestUrl)
            .then(response => response.json())
            .then(data => dispatch(setCategories(data.categories)))
            .catch(() => dispatch(setErrorMessage('Unexpected error occurred')));
    };
};