import {Dispatch} from 'redux';
import {ACTION_TYPES} from '../reducers/flowReducer';
import {IFlow} from '../models/IFlow';

export const setFlowAction = (flow: IFlow, categoryName: string) => async (dispatch: Dispatch) => {
    return dispatch({
        type: ACTION_TYPES.SET_FLOW,
        payload: {flow, categoryName}
    });
}

export const makeAction = (flow: IFlow, actionName: string) => async (dispatch: Dispatch) => {
    return dispatch({
        type: ACTION_TYPES.SET_ACTION_FLOW,
        payload: {flow, actionName}
    });
}

export const resetActionFlow = () => ({
    type: ACTION_TYPES.RESET_ACTION_FLOW
});
