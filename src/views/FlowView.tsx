import React from 'react';
import {ActionsButtonsWrapper} from '../components/ActionsButtonsWrapper/ActionsButtonsWrapper';
import {useSelector} from 'react-redux';
import {IRootState} from '../models/IRootState';
import {Card} from 'react-bootstrap';
import '../styles/FlowView.scss';

export const FlowView: React.FC = () => {
    const flow = useSelector((state: IRootState) => state.flow.data);
    const category = useSelector((state: IRootState) => state.flow.categoryName);

    return (
        <div className="flow-view">
            <Card>
                <Card.Body>
                    <h2>Flow name: {flow.name || '-'} </h2>
                    <Card.Title>
                        Flow code: {flow.code}
                    </Card.Title>
                    <Card.Subtitle>
                        Category: {category}
                    </Card.Subtitle>
                    <ActionsButtonsWrapper/>
                </Card.Body>
            </Card>
        </div>
    )
}