import React, {useCallback, useEffect, useState} from 'react';
import {Menu} from '../components/Menu/Menu';
import {useDispatch, useSelector} from 'react-redux';
import {getCategoriesAction} from '../actions/categoryActions';
import {IRootState} from '../models/IRootState';
import {isEmpty} from 'lodash';
import {Alert} from '../components/Alert/Alert';
import {resetActionFlow} from '../actions/flowActions';
import {FlowView} from './FlowView';
import {ToastError} from '../components/ToastError/ToastError';

const Home: React.FC = () => {
    const dispatch = useDispatch();
    const categories = useSelector((state: IRootState) => state.categories.data);
    const flow = useSelector((state: IRootState) => state.flow.data);
    const actionName = useSelector((state: IRootState) => state.flow.actionName)
    const [alertVisible, setAlertVisible] = useState(false);

    useEffect(() => {
        dispatch(getCategoriesAction())
    }, [dispatch])

    const handleCloseAlert = useCallback(() => {
        setAlertVisible(false);
        // setTimeout is for correct animation from the alert.
        setTimeout(() => dispatch(resetActionFlow()), 200)
    }, [setAlertVisible, dispatch])

    useEffect(() => {
        setAlertVisible(!!actionName)
    }, [setAlertVisible, actionName])

    return (
        <div>
            <Menu menuItems={categories}/>
            {!isEmpty(flow) && <FlowView/>}
            <Alert show={alertVisible} onHide={handleCloseAlert} title={`Flow code: ${flow.code}`}
                   text={`Action name: ${actionName}`}/>
            <ToastError/>
        </div>
    )
}

export default Home;