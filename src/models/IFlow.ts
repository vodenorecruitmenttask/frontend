export interface IFlow {
    code?: string;
    name?: string;
}