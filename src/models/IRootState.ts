import {FlowReducerState} from "../reducers/flowReducer";
import {CategoryReducerState} from "../reducers/categoryReducer";

export interface IRootState {
    categories: CategoryReducerState
    flow: FlowReducerState,
}