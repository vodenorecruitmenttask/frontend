export interface IAction {
    readonly type?: string;
    readonly payload?: any;
    error?: boolean;
    meta?: any;
}