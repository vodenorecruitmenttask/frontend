import React from "react";

export interface IButtonDetail {
    actionName: string;
    icon?: React.ReactNode;
    text?: string;
    handleOnClick: Function;
}