import {IFlow} from "./IFlow";

export interface ICategory {
    title: string;
    flows: IFlow[];
}