import {Toast} from 'react-bootstrap';
import React, {useCallback, useEffect, useState} from 'react';
import '../../styles/Toast.scss';
import {useSelector} from 'react-redux';
import {IRootState} from '../../models/IRootState';

export const ToastError: React.FC = () => {
    const errorMessage = useSelector((state: IRootState) => state.categories.error)
    const [isVisible, setIsVisible] = useState(false);

    const handleClose = useCallback(() => {
        setIsVisible(false);
    }, [setIsVisible]);

    useEffect(() => {
        setIsVisible(!!errorMessage);
    }, [errorMessage, setIsVisible]);

    return (
        <Toast onClose={handleClose} className="toast" show={isVisible} autohide delay={3000}>
            <Toast.Header>
                <strong className="mr-auto text-danger">{errorMessage}</strong>
            </Toast.Header>
        </Toast>
    )
}