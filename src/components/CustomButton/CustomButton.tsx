import React from 'react';
import {Button} from 'react-bootstrap';
import {IButtonDetail} from '../../models/IButtonDetail';

interface ICustomButtonProps extends IButtonDetail {}

export const CustomButton:React.FC<ICustomButtonProps> = ({actionName, icon, text, handleOnClick}: ICustomButtonProps) => {
    return (
        <Button variant="info" onClick={handleOnClick(actionName)}>
            { text }
            { icon }
        </Button>
    )
}