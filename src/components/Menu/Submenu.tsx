import React, {useCallback, useMemo} from 'react';
import {Dropdown} from 'react-bootstrap';
import {ICategory} from '../../models/ICategory';
import {useDispatch} from 'react-redux';
import {setFlowAction} from '../../actions/flowActions';
import {IFlow} from '../../models/IFlow';

interface ISubmenuProps {
    item: ICategory;
    id: number;
}

export const Submenu: React.FC<ISubmenuProps> = ({item, id}: ISubmenuProps) => {
    const dispatch = useDispatch();

    const handleOnClickSubmenu = useCallback((flow: IFlow) => () => {
        dispatch(setFlowAction(flow, item.title))
    }, [dispatch, item.title]);

    const displayName = useMemo(() => `${id}. ${item.title}`, [id, item.title]);

    return (
        <Dropdown drop="right">
            <Dropdown.Toggle id={`submenu-${item.title}`}>{displayName}</Dropdown.Toggle>
            <div className="submenu">
                <Dropdown.Menu>
                    {
                        item.flows.map((flow, i) =>
                            <Dropdown.Item
                                onClick={handleOnClickSubmenu(flow)}
                                eventKey={`${item.title}-${flow.code}-${i.toString()}`}
                                key={i}>{flow.name || flow.code}
                            </Dropdown.Item>)
                    }
                </Dropdown.Menu>
            </div>
        </Dropdown>
    )
}