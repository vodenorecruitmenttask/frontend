import React from 'react';
import {Nav, Navbar, NavDropdown} from 'react-bootstrap';
import {Submenu} from './Submenu';
import '../../styles/Menu.scss';

interface IMenuProps {
    menuItems: any[];
}

export const Menu: React.FC<IMenuProps> = ({menuItems = []}: IMenuProps) => {
    return (
        <div className="menu">
            <Navbar collapseOnSelect expand="lg">
                <Navbar.Toggle aria-controls="navbar-nav"/>
                <Navbar.Collapse id="navbar-nav">
                    <Nav className="mr-auto">
                        <div className="main-menu">
                            <NavDropdown title="Menu" id="nav-dropdown">
                                {
                                    menuItems.map((category, i) =>
                                        <Submenu id={i} item={category} key={i}/>
                                    )
                                }
                            </NavDropdown>
                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}