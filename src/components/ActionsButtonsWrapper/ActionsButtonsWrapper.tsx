import React, {useCallback} from 'react';
import {CustomButton} from '../CustomButton/CustomButton';
import {Files, XCircle, Square, HddStack} from 'react-bootstrap-icons/dist';
import {useDispatch, useSelector} from 'react-redux';
import {makeAction} from '../../actions/flowActions';
import {IRootState} from '../../models/IRootState';
import '../../styles/ActionsButtonsWrapper.scss';

export const ActionsButtonsWrapper: React.FC = () => {
    const dispatch = useDispatch();
    const flow = useSelector((state: IRootState) => state.flow.data);
    const buttonsDetails = [
        {actionName: 'SCE Open with this tree', text: 'SCE'},
        {actionName: 'Link to docum. (API Portal)', text: 'API'},
        {actionName: 'Open postman form Saved on Schema IN', icon: <HddStack/>},
        {actionName: 'Make a copy', icon: <Files/>},
        {actionName: 'Delete', icon: <XCircle/>},
        {actionName: 'New', icon: <Square/>},
    ]

    const handleOnClickActionButton = useCallback((actionCode: string) => () => {
        dispatch(makeAction(flow, actionCode));
    }, [dispatch, flow])

    return (
        <div className="actions-buttons-wrapper">
            {
                buttonsDetails.map((button, i) =>
                    <CustomButton
                        key={i}
                        actionName={button.actionName}
                        icon={button.icon}
                        text={button.text}
                        handleOnClick={handleOnClickActionButton}
                    />
                )
            }
        </div>
    )
}