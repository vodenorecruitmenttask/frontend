import React from 'react';
import {Button, Modal} from 'react-bootstrap';

interface IAlertProps {
    title: string;
    text: string;
    onHide: () => void;
    show: boolean;
}

export const Alert: React.FC<IAlertProps> = ({text, title, onHide, show}: IAlertProps) => {
    return (
        <Modal
            onHide={onHide}
            show={show}
            size="lg"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="action-modal">
                    {title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    {text}
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    )
}