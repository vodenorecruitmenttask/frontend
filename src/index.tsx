import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import configureStore from './store/store';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <Provider store={configureStore()}>
        <Suspense fallback={<div className="loading"/>}>
            <App/>
        </Suspense>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
